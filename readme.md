![ZAPPlication Logo ](images/zapp-logo.png)
## ZAPP Jaspersoft Reports
ZAPP Jaspersoft Reports Embedded Example

***

### Builit With
* [Font Awesome Pro 5.8.2](https://fontawesome.com/start)
* [Bootstrap 4](https://getbootstrap.com/)

***

### Authors
Created by [WESTAF - Western States Art Federation](http://westaf.org)
![WESTAF Logo ](images/westaf-logo.png)

***

##### Versioning
[![version](https://img.shields.io/badge/version-0.1.0-yellow.svg)](https://semver.org)

***

### License
[MIT](https://choosealicense.com/licenses/mit/)

***


[How to Embed via ZOHO](https://www.zoho.com/creator/help/reports/publish-and-embed-reports.html )
[How to Embed via Jaspersoft](https://community.jaspersoft.com/wiki/visualizejs-tutorials)

***